package cryptography;

import controllers.MainAppController;

import javax.crypto.*;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

public class Encryption {
    static final char[] keyStorePassword = "CodeProtector".toCharArray();
    static final String pathToKeyStore = "codeProtectorKeyStore.jks";

    public static void encrypt(File sourceFile, String username, String destinationUser, Certificate signCert)throws NoSuchPaddingException, NoSuchAlgorithmException,InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnrecoverableKeyException, NoSuchProviderException, CertificateException, SignatureException, IOException, KeyStoreException {
        signCert.verify(LoginService.caPublicKey);
        final String destinationUserInbox = "Users" + File.separatorChar + destinationUser + File.separatorChar + "Inbox";
        byte[] sourceCode = new BufferedInputStream(new FileInputStream(sourceFile)).readAllBytes();
        KeyStore pkcs12 = KeyStore.getInstance("PKCS12");
        pkcs12.load(new FileInputStream(new File("Users" + File.separatorChar + username + File.separatorChar + username+ "Sign.pfx")), MainAppController.userPassword.toCharArray());
        PrivateKey key = (PrivateKey)pkcs12.getKey("1", MainAppController.userPassword.toCharArray());
        Signature sourceCodeSignature = Signature.getInstance("SHA256withDSA");
        sourceCodeSignature.initSign(key);
        sourceCodeSignature.update(sourceCode);
        byte[] signedSource = sourceCodeSignature.sign();
        //Generate Symmetric key
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecretKey aesKey = keyGenerator.generateKey();
        //Read Certificate from KeyStore
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream(pathToKeyStore), keyStorePassword);
        Certificate userDestCert = keyStore.getCertificate(destinationUser + "auth");
        userDestCert.verify(LoginService.caPublicKey);
        //Get Destination User's public RSA Key for symmetric key encryption
        RSAPublicKey publicKey = (RSAPublicKey)userDestCert.getPublicKey();
        Cipher rsa = Cipher.getInstance("RSA");
        rsa.init(Cipher.ENCRYPT_MODE, publicKey);
        //Encrypt key
        byte[] encryptedKey = rsa.doFinal(aesKey.getEncoded());
        Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
        aes.init(Cipher.ENCRYPT_MODE, aesKey);
        //Encrypt fileName
        String info = Base64.getEncoder().encodeToString(aes.doFinal(username.getBytes())) + ":" + Base64.getEncoder().encodeToString(encryptedKey) + ":" +Base64.getEncoder().encodeToString(signedSource);
        String encryptedFileName = Base64.getEncoder().encodeToString(aes.doFinal(sourceFile.getName().getBytes())).replace('/', '$');
        PrintWriter writer = new PrintWriter(new FileWriter( destinationUserInbox+ File.separatorChar + encryptedFileName + "_info.txt"));
        writer.write(info);
        writer.close();
        //Digital sign information
        Signature sign = Signature.getInstance("SHA256withDSA");

        sign.initSign(key);
        sign.update(info.getBytes());
        byte[] signature = sign.sign();
        writer = new PrintWriter(new FileWriter(destinationUserInbox + File.separatorChar + encryptedFileName + "_signature.txt"));
        writer.write(Base64.getEncoder().encodeToString(signature));
        writer.close();
        //Encrypt file
        byte[] cipherText = aes.doFinal(sourceCode);
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(destinationUserInbox + File.separatorChar + encryptedFileName + ".enc")));
        stream.write(cipherText);
        stream.close();
        InboxChecker.calculateHash(destinationUser);
    }
}