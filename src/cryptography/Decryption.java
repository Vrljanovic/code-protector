package cryptography;

import codeRunner.CodeRunner;
import controllers.MainAppController;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;

public class Decryption {
    public static void decrypt(File encryptedFile)throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, KeyStoreException, CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException, IllegalBlockSizeException, BadPaddingException{
        final String userInbox = "Users" + File.separatorChar + MainAppController.username + File.separatorChar + "Inbox";
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(encryptedFile));
        byte[] encrypted = stream.readAllBytes();
        stream.close();
        Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
        Cipher rsa = Cipher.getInstance("RSA");
        File infoFile = new File(userInbox + File.separatorChar + encryptedFile.getName().replace(".enc", "") + "_info.txt");
        //Read source of message, encrypted symmetric key, original file hash
        BufferedReader reader = new BufferedReader(new FileReader(infoFile));
        String info = reader.readLine();
        String[] information = info.split(":");
        reader.close();
        byte[] key = Base64.getDecoder().decode(information[1]);
        //Read user's private key
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream(new File("Users" + File.separatorChar + MainAppController.username + File.separatorChar + MainAppController.username + "auth.pfx")), MainAppController.userPassword.toCharArray());
        PrivateKey privateKey = (PrivateKey) keyStore.getKey("1", MainAppController.userPassword.toCharArray());
        //Decrypt symmetric key and file with symmetric key
        rsa.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] aesKey = rsa.doFinal(key);
        aes.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aesKey, "AES"));
        byte[] decryptedText = aes.doFinal(encrypted);
        byte[] messageSupplier = aes.doFinal(Base64.getDecoder().decode(information[0]));
        String messageFrom = new String(messageSupplier);
        //Verify source file signature
        byte[] sourceSignature = Base64.getDecoder().decode(information[2]);
        keyStore.load(new FileInputStream(Encryption.pathToKeyStore), Encryption.keyStorePassword);
        PublicKey publicKey = keyStore.getCertificate(messageFrom).getPublicKey();
        Signature sourceCodeSignature = Signature.getInstance("SHA256withDSA");
        sourceCodeSignature.initVerify(publicKey);
        sourceCodeSignature.update(decryptedText);
        if (!sourceCodeSignature.verify(sourceSignature))
            throw new SignatureException();
        //Read sender's public key
        //Verify signature of information file
        byte[] infoBytes = info.getBytes();
        Signature signature = Signature.getInstance("SHA256withDSA");
        signature.initVerify(publicKey);
        signature.update(infoBytes);
        File signatureFile = new File(userInbox + File.separatorChar + encryptedFile.getName().replace(".enc", "") + "_signature.txt");
        stream = new BufferedInputStream(new FileInputStream(signatureFile));
        if (!signature.verify(Base64.getDecoder().decode(new String(stream.readAllBytes()))))
            throw new SignatureException();
        stream.close();
        //Decrypt file name
        String file = encryptedFile.getName().replace("$", "/");
        file = file.replaceAll(".enc", "");
        byte[] fileName = Base64.getDecoder().decode(file);
        String decryptedFileName = new String(aes.doFinal(fileName));
        //If file is not changed write decrypted data
        Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK);
        alert.setTitle("Information");
        alert.setContentText("Message from " + messageFrom + ".");
        alert.show();
        File decryptedFile = new File(userInbox + File.separatorChar + decryptedFileName);
        if(!decryptedFile.exists())
            decryptedFile.createNewFile();
        PrintWriter writer = new PrintWriter(new FileWriter(decryptedFile));
        CodeRunner.compileAndRun(decryptedFile);
        writer.print(new String(decryptedText, StandardCharsets.UTF_8));
        writer.close();
        signatureFile.delete();
        encryptedFile.delete();
        infoFile.delete();
        InboxChecker.calculateHash(MainAppController.username);
    }
}