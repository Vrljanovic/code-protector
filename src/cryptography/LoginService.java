package cryptography;

import controllers.MainAppController;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;
public class LoginService {
    public static final String caCertPath= "CA Root" + File.separatorChar + "certs" + File.separatorChar + "CA_CodeProtector.cer";
    static PublicKey caPublicKey;

    public static boolean checkCredentials(String username, String password, String pathToCertAuth, String pathToSignCert) throws NoSuchAlgorithmException, IOException, CertificateException, KeyStoreException, NoSuchProviderException, SignatureException, InvalidKeyException {
        //Check username and password
        String userHomePath = "Users" + File.separatorChar + username + File.separatorChar;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        byte[] hash = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
        String passwordHash = Base64.getEncoder().encodeToString(hash);
        BufferedReader reader = new BufferedReader(new FileReader(userHomePath + username + ".txt"));
        String usernameInFile = reader.readLine().split(":")[1];
        String passwordHashInFile = reader.readLine().split(":")[1];
        reader.close();
        // Load CA Public Key
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        Certificate caCert = factory.generateCertificate(new FileInputStream(caCertPath));
        caPublicKey = caCert.getPublicKey();

        KeyStore pkcs12 = KeyStore.getInstance("PKCS12");
        //Load Users Authentication Certificate
        pkcs12.load(new FileInputStream(pathToCertAuth), password.toCharArray());
        Certificate userAuthCert = pkcs12.getCertificate("1");
        userAuthCert.verify(caPublicKey);

        MainAppController.userAuthCert = userAuthCert;
        pkcs12.load(new FileInputStream(pathToSignCert), password.toCharArray());
        MainAppController.userSignCert = pkcs12.getCertificate("1");
        return username.equals(usernameInFile) && passwordHash.equals(passwordHashInFile);
    }
}
