package controllers;

import cryptography.InboxChecker;
import cryptography.LoginService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.*;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;


public class LoginController {
    static Stage stage;

    @FXML
    private TextField usernameTextField;

    @FXML
    private PasswordField passwordTextField;

    @FXML
    private Button loginButton;

    @FXML
    private TextField authCertTextField;

    @FXML
    private TextField signCertTextField;

    @FXML
    private void login() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("UserDirCheck" + File.separatorChar +  "CA_CodeProtector.txt"));
            String oldHash = reader.readLine();
            reader.close();
            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(LoginService.caCertPath));
            byte[] cacert = stream.readAllBytes();
            stream.close();
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(cacert);
            if(!Base64.getEncoder().encodeToString(hash).equals(oldHash)){
                printErrorMessage("CA Certificate Error!");
                System.exit(0);
            }
            if (LoginService.checkCredentials(usernameTextField.getText(), passwordTextField.getText(), authCertTextField.getText(), signCertTextField.getText())) {
                if (!InboxChecker.validateDir(usernameTextField.getText())) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, null, ButtonType.OK);
                    alert.setContentText("Somebody manually changed your inbox.");
                    alert.show();
                }
                MainAppController.username = usernameTextField.getText();
                MainAppController.userPassword = passwordTextField.getText();
                Parent root = FXMLLoader.load(getClass().getResource("mainAppForm.fxml"));
                Scene scene = new Scene(root);
                Stage stage = (Stage) loginButton.getScene().getWindow();
                stage.setScene(scene);
                MainAppController.stage = stage;
                stage.setTitle("Code Protector");
                stage.centerOnScreen();
                stage.setOnCloseRequest(windowEvent -> {
                    try {
                        InboxChecker.calculateHash(usernameTextField.getText());
                    }catch(Exception ex){
                        printErrorMessage("Could not store your inbox");
                    }
                });
                stage.show();
            } else {
                printErrorMessage("Username and password don't match.");
                MainAppController.userSignCert = null;
                MainAppController.userAuthCert = null;
            }
        }catch(CertificateException ex){
            printErrorMessage("Certificate is not valid.");
        }catch (SignatureException ex){
            printErrorMessage("Signature not valid.");
        }catch(KeyStoreException ex) {
            printErrorMessage("Wrong password.");
        }catch(IOException ex) {
            printErrorMessage("Wrong combination of username and password or something is wrong with certificates you provided.");
        }catch (Exception ex){
            printErrorMessage("Unexpected Error");
        }
    }

    @FXML
    private void browseCert(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File userHome = new File("Users" + File.separatorChar + usernameTextField.getText());
        if (userHome.exists() && userHome.isDirectory())
            fileChooser.setInitialDirectory(userHome);
        else {
            fileChooser.setInitialDirectory(new File("Users"));
        }
        if (((Button) event.getSource()).getId().equals("browseAuthCertButton")) {
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Authentication Certificate File", "*Auth.pfx"));
            File file = fileChooser.showOpenDialog(stage);
            if (file != null)
                authCertTextField.setText(file.getAbsolutePath());
        } else {
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Sign Certificate File", "*Sign.pfx"));
            File file = fileChooser.showOpenDialog(stage);
            if (file != null)
                signCertTextField.setText(file.getAbsolutePath());
        }
    }

    private static void printErrorMessage(String errorMessage){
        Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
        alert.setTitle("Error");
        alert.setContentText(errorMessage);
        alert.showAndWait();
    }
}
