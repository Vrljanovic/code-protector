package codeRunner;

import controllers.MainAppController;
import java.io.*;

public class CodeRunner {
    private static final Runtime runtime = Runtime.getRuntime();

    public static void compileAndRun(File file) {
        new Thread(()-> {
            String compileCommand = "";
            String line = "";
            String MainClassName = "";
            String runCommand = "";
            if (file.getName().endsWith(".java")) {
                MainClassName = file.getName().replace(".java", "");
                compileCommand = "javac " + "\"" + file.getPath() + "\"";
                runCommand = "java -cp \"" + file.getParentFile().getPath() + "\" " + MainClassName;
            } else {
                MainAppController.showProgramOutput("Source file is not java.");
                return;
            }
            try {
                Process compileProcess = runtime.exec(compileCommand);
                BufferedReader compileErrorReader = new BufferedReader(new InputStreamReader(compileProcess.getErrorStream()));
                while ((line = compileErrorReader.readLine()) != null)
                    MainAppController.showProgramOutput(line);
                compileErrorReader.close();
                compileProcess.waitFor();
                if (compileProcess.exitValue() == 0) {
                    Process runProcess = runtime.exec(runCommand);
                    BufferedReader runInputReader = new BufferedReader(new InputStreamReader(runProcess.getInputStream()));
                    BufferedReader runErrorReader = new BufferedReader(new InputStreamReader(runProcess.getErrorStream()));
                    String print = "";
                    while ((print = runInputReader.readLine()) != null)
                        MainAppController.showProgramOutput(print);
                    runInputReader.close();
                    while ((print = runErrorReader.readLine()) != null)
                        MainAppController.showProgramOutput(print);
                    runErrorReader.close();
                    runProcess.waitFor();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }).start();
    }
}