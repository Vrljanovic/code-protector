package cryptography;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class InboxChecker {
    public static void calculateHash (String username) throws IOException, NoSuchAlgorithmException {
        File directory = new File("Users" + File.separatorChar + username + File.separatorChar +  "Inbox");
        String[] fileList = directory.list();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        String hash = "$";
        for(String fileName : fileList)
            hash += Base64.getEncoder().encodeToString(digest.digest(fileName.getBytes()));
        PrintWriter writer = new PrintWriter(new FileWriter(System.getProperty("user.home") + File.separatorChar + "CodeProtector" + File.separatorChar + username + ".txt"));
        writer.write(hash);
        writer.close();
    }

    public static boolean validateDir(String username)throws IOException, NoSuchAlgorithmException{
        File directory = new File("Users" + File.separatorChar + username + File.separatorChar + "Inbox");
        String[] files = directory.list();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.home") + File.separatorChar + "CodeProtector" + File.separatorChar + username + ".txt"));
        String hash = reader.readLine();
        reader.close();
        String calculatedHash ="$";
        for(String filename : files)
            calculatedHash += Base64.getEncoder().encodeToString(digest.digest(filename.getBytes()));
        return calculatedHash.equals(hash);
    }

}
