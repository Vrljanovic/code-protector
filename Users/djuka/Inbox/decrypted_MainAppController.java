package controllers;

import cryptography.Decryption;
import cryptography.Encryption;
import cryptography.InboxChecker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.crypto.IllegalBlockSizeException;
import java.io.*;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ResourceBundle;

public class MainAppController implements Initializable {
    public static Certificate userAuthCert;
    public static Certificate userSignCert;
    public static String username;
    public static String userPassword;
    static Stage stage;

    @FXML
    private TextField fileToEncryptTextField;

    @FXML
    private TextField fileToDecryptTextField;

    @FXML
    private Button logoutButton;

    @FXML
    private ComboBox<String> allUsersComboBox;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        File usersHome = new File("Users");
        String[] allUsers = usersHome.list();
        if(allUsers != null)
           allUsersComboBox.getItems().addAll(allUsers);
    }

    public void browseFile(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        if(((Button) event.getSource()).getId().equals("browseFileToEncryptButton")) {
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("File to encrypt", "*"));
            File file = fileChooser.showOpenDialog(stage);
            if(file != null)
                 fileToEncryptTextField.setText(file.getAbsolutePath());
        }
        else {
            fileChooser.setInitialDirectory(new File("Users" + File.separatorChar + username +  File.separatorChar + "Inbox"));
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("File to decrypt", "*.enc"));
            File file = fileChooser.showOpenDialog(stage);
            if(file != null)
                 fileToDecryptTextField.setText(file.getAbsolutePath());
        }
    }

    public void encrypt(){
        try {
            Encryption.encrypt(new File(fileToEncryptTextField.getText()), username, allUsersComboBox.getSelectionModel().getSelectedItem(), userSignCert);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK);
            alert.setContentText("Message has been encrypted and sent");
            alert.show();
        }catch(SignatureException ex){
            printErrorAlert("Signature Error");
        }catch(InvalidKeyException ex){
            printErrorAlert("Invalid Key");
        }catch(CertificateException ex){
            printErrorAlert("Certificate Error");
        }catch(Exception ex){
            printErrorAlert("Unexpected Error");
        }
    }

    public void decrypt(){
        try {
            Decryption.decrypt(new File(fileToDecryptTextField.getText()));
        }catch(IllegalBlockSizeException ex){
            printErrorAlert("Encrypted Message is not valid");
        }catch(InvalidKeyException ex){
            printErrorAlert("Invalid Key");
        }catch(IOException ex){
            printErrorAlert("There is to few files for decryption");
        }catch (SignatureException ex){
            printErrorAlert("CodeProtector could not verify signature, or message has been changed");
        }catch(Exception ex){
            printErrorAlert("Unexpected error");
        }
    }

    private static void printErrorAlert(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
        alert.setContentText(message);
        alert.show();
    }

    public void logout(){
        try {
            InboxChecker.calculateHash(username);
        }catch(Exception ex){
            printErrorAlert("Could not store your inbox information.");
        }
        username = null;
        userPassword = null;
        userSignCert = null;
        userAuthCert = null;
        allUsersComboBox = null;
        try {
            Parent root = FXMLLoader.load(getClass().getResource("loginForm.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage)logoutButton.getScene().getWindow();
            stage.setScene(scene);
            MainAppController.stage = null;
            stage.setTitle("Code Protector");
            stage.centerOnScreen();
            stage.show();
        }catch(Exception  ex){
            ex.printStackTrace();
        }
    }
}