public class Main{
	public static void main(String[] args){	
		int i = 2;
		System.out.println("Prvih 15 prostih brojeva:");
		int j = 0;
		while(j != 15){
			if(isPrime(i)){
				System.out.println(i);
				++j;
			}
			++i;
		}
	}
	
	public static boolean isPrime(int number){
		for(int i = 2; i <=  Math.sqrt(number); ++i)
		{
			if(number % i == 0)
				return false;
		}
		return true;
	}
}